all: index.html

%.html: %.md header.html Makefile
	pandoc \
            --standalone \
            -o $@ \
            -t revealjs \
            --slide-level=2 \
            --shift-heading-level-by=1 \
            -H header.html \
            -V theme=solarized \
            -V revealjs-url=https://unpkg.com/reveal.js@4.2.1 \
            $<

# For an offline version:
# yarn add reveal.js
# Then set -V revealjs-url=./node_modules/reveal.js
