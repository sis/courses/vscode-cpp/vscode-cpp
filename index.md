---
author: Gerhard Bräunlich
date: January 2022
lang: en-US
title: VS Code for Git, GitLab and C++
...

# Setup {#setup}

|     |                                                                                   |
|-----|-----------------------------------------------------------------------------------|
| 🐧  | [Visual Studio Code on Linux](https://code.visualstudio.com/docs/setup/linux)     |
| 🪟  | [Visual Studio Code on Windows](https://code.visualstudio.com/docs/setup/windows) |
| 🍏  | [Visual Studio Code on macOS](https://code.visualstudio.com/docs/setup/mac)       |

Inofficial ([FOSS](https://itsfoss.com/vscodium/)) alternative:
[vscodium](https://vscodium.com/).

Note on privacy: [Disable telemetry](#telemetry)


# Basic features

::: r-stack
:::{.fragment .fade-in-then-out}
![Bracket Matching](img/bracket-matching.png)
:::
:::{.fragment .fade-in-then-out}
![Code Navigation](img/code-navigation.gif)
:::
:::{.fragment .fade-in-then-out}
![Rename Symbol](img/rename-symbol.gif)
:::
:::{.fragment .fade-in-then-out}
![Integrated Terminal](img/integrated-terminal.png)
:::
:::

## Quick Open: `Ctrl+P` / `⌘+P`

::: compact

| Start char | Effect          |                                               |
|------------|-----------------|-----------------------------------------------|
|            | Open file       | ![](./img/quick-open-file.png)                |
| `>`        | Command palette | ![](./img/quick-open-command-palette.png)     |
| `@`        | Symbol lookup   | ![](./img/quick-open-symbol-lookup.png)       |
| `#`        | Symbol lookup   | ![](./img/quick-open-symbol-lookup-sharp.png) |
| `:`        | Jump to line    | ![](./img/quick-open-jump-to-line.png)        |

:::

## Config{#settings}

Via command palette: `Ctrl+⇧+P` / `⌘+⇧+P` ![](img/open-settings.png)

![](img/settings.png)

---

Config path (share / backup config):

```
$HOME/.config/Code/User/settings.json # 🐧
```

```
%APPDATA%\Code\User\settings.json # 🪟
```

```
$HOME/Library/Application Support/Code/User/settings.json # 🍏
```


Per project config:

```
.vscode/settings.json
```

# Install extensions

Quick Open (`Ctrl+P` / `⌘+P`) -> `ext install`{.prompt} -> Enter.

Then search by keyword and browse.

::: fragment
Using the extension id: Quick Open (`Ctrl+P` / `⌘+P`) -> `ext install <extension-id>`{.prompt}
-> Enter.
:::

# gitlab integration

* View issues.
* Create and review merge requests.
* Validate your GitLab CI configuration.
* View the status of your pipeline.
* Create and paste snippets to, and from, your editor.

```
ext install gitlab.gitlab-workflow
```

Then follow [their
instructions](https://gitlab.com/gitlab-org/gitlab-vscode-extension#setup)

Possible issue: [certificates]{#cert-issue}

---

![](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/raw/d0589878829338b64657e592f3451f1dace41cdf/docs/assets/sidebar.gif)

# C++ integration
## C++ tools

* Code completion
* Code navigation
* Debugging

```
ext install ms-vscode.cpptools # ⚠️ proprietary
```

---

![](https://code.visualstudio.com/assets/docs/languages/cpp/msg-intellisense.png)

---

## Auto formatting

Requires: clang-format

::: large

|    |                                                                  |
|----|------------------------------------------------------------------|
| 🐧 | `sudo apt-get install clang-format`{.prompt}                              |
| 🍏 | `brew install clang-format`{.prompt}                                      |
| 🪟 | [instructions](https://www.wikihow.com/Install-Clang-on-Windows) |

:::

[Settings](#settings):

```json
{
    "editor.formatOnSave": true
}
```

# CMake integration
## CMake tools

```sh
ext install ms-vscode.cmake-tools
```

- initial configuration,
  ![](https://code.visualstudio.com/assets/docs/cpp/cpp/cmake-quickstart-command-palette.png)

- run / debug build / change build type
  ![](img/cmake-bar.png)
- autobuild on configuration changes,
- autocompletion of CMake commands and vars,

# Working remote
## Remote SSH

```sh
ext install ms-vscode-remote.remote-ssh # ⚠️ proprietary
```

Requires: [Setup of SSH keys](https://code.visualstudio.com/docs/remote/ssh-tutorial#_set-up-ssh)

---

![](https://microsoft.github.io/vscode-remote-release/images/ssh-readme.gif)

# Working in a container
## Remote container

```sh
ext install ms-vscode-remote.remote-containers # ⚠️ proprietary
```

Requires: [docker](https://code.visualstudio.com/docs/remote/containers#_system-requirements)

---

![](https://microsoft.github.io/vscode-remote-release/images/remote-containers-readme.gif)

# Other useful extensions

## Code Spell Checker

```sh
ext install streetsidesoftware.code-spell-checker
```

## Doxygen Documentation Generator

```sh
ext install cschlosser.doxdocgen
```

## C/C++ Extension Pack

```sh
ext install ms-vscode.cpptools-extension-pack
```

Contains:

* C/C++
* C/C++ Themes
* CMake
* CMake Tools
* Doxygen Documentation Generator
* Better C++ Syntax
* Remote Development Extension Pack

# Exercises

[https://gitlab.ethz.ch/sis/courses/vscode-cpp/exercises/cpp-gitlab-workflow](https://gitlab.ethz.ch/sis/courses/vscode-cpp/exercises/cpp-gitlab-workflow)

* Exercise 1: [Create / publish a new C++ project](https://gitlab.ethz.ch/sis/courses/vscode-cpp/exercises/cpp-gitlab-workflow/-/blob/main/01.md)
* Exercise 2: [Modify the project / review changes](https://gitlab.ethz.ch/sis/courses/vscode-cpp/exercises/cpp-gitlab-workflow/-/blob/main/02.md)

# {data-background-iframe="https://scicomp.ethz.ch/wiki/VSCode"}

::: label-box
Setup euler for ssh-remote
[scicomp.ethz.ch/wiki/VSCode](https://scicomp.ethz.ch/wiki/VSCode)
:::

# FAQ

## Disable telemetry{#telemetry}

Settings > Search "telementry" > Telemetry: Enable Telemetry

Also see: [code.visualstudio.com/docs/getstarted/telemetry](https://code.visualstudio.com/docs/getstarted/telemetry)

## Certificate Issue{#cert-issue}

If you get

```
request to https://gitlab.ethz.ch/api/graphql failed, reason:
certificate has expired
```

Set

```json
{
"http.systemCertificates": false
}
```

in the config.

## Install proprietary extensions in VS Codium

**C++ tools**: Grab a release
[here](https://github.com/Microsoft/vscode-cpptools/releases) and use

```{.sh .stretch}
codium --install-extension downloads/folder/cpptools-linux.vsix
```

**Remote ssh**: Grab a release from
[here](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)

```sh
codium --install-extension \
  downloads/ms-vscode-remote.remote-ssh-*.vsix
```

**Remote container**: Grab a release from
[here](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

```sh
codium --install-extension \
  downloads/ms-vscode-remote.remote-containers-*.vsix
```

---

VS Code Marketplace:
[https://marketplace.visualstudio.com](https://marketplace.visualstudio.com/)

VS Codium repository: [https://open-vsx.org](https://open-vsx.org/)


# Acknowledgements

Slides:

* [Reveal.js](https://revealjs.com/), MIT
* [Pandoc](https://pandoc.org/), GPLv2+, BSD 3-clause

Mik Rybinski and Manuel Weberndorfer for careful reviews and suggestions.
