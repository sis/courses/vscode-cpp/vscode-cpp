# VS Code for C++

Goal:
* Integrate VS Code into the git workflow,
* extend VS Code to the needs of a C++ project.

We will start with a simple C++ test project.
At the end, from within VS Code, you should be able to
* see syntax highlighting,
* have automatic code formatting working,
* compile the project (running CMake behind the scenes),
* compile code remotely,
* review, commit and synchronise your changes using git.

# Pre-Requisites

* git ([Installing
git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git))
* VS Code (see [below](#setup))

